FROM rust:1.59
WORKDIR /vp
COPY . .
RUN cargo build --release
EXPOSE 31421
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./target/release/vp_server"]