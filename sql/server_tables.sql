-- Table: server.auth

-- DROP TABLE server.auth;

CREATE TABLE IF NOT EXISTS server.auth
(
    key character varying(64) COLLATE pg_catalog."default" NOT NULL,
    name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT auth_pkey PRIMARY KEY (key)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE server.auth
    OWNER to voiceprofiles;