-- Table: vp.voiceprofile

-- DROP TABLE vp.voiceprofile;

CREATE TABLE IF NOT EXISTS vp.voiceprofile
(
    id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    guild_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    experience bigint NOT NULL,
    voicepoints bigint NOT NULL,
    "level" bigint NOT NULL,
    data text,
    CONSTRAINT voiceprofile_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE vp.voiceprofile
    OWNER to voiceprofiles;

-- Table: vp.pray

-- DROP TABLE vp.pray;

CREATE TABLE IF NOT EXISTS vp.pray
(
    voiceprofile_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    date bigint NOT NULL,
    streak bigint NOT NULL,
    total bigint NOT NULL,
    CONSTRAINT pray_pkey PRIMARY KEY (voiceprofile_id),
    CONSTRAINT voiceprofile_id FOREIGN KEY (voiceprofile_id)
        REFERENCES vp.voiceprofile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE vp.pray
    OWNER to voiceprofiles;

-- Table: vp.timespent

-- DROP TABLE vp.timespent;

CREATE TABLE IF NOT EXISTS vp.timespent
(
    voiceprofile_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    place character varying(64) COLLATE pg_catalog."default" NOT NULL,
    "time" bigint NOT NULL,
    CONSTRAINT voiceprofile_id FOREIGN KEY (voiceprofile_id)
        REFERENCES vp.voiceprofile (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE vp.timespent
    OWNER to voiceprofiles;

-- Table: vp.godprofile

-- DROP TABLE vp.godprofile;

CREATE TABLE IF NOT EXISTS vp.godprofile
(
    voiceprofile_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT godprofile_pkey PRIMARY KEY (voiceprofile_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE vp.godprofile
    OWNER to voiceprofiles;

-- Table: vp.transaction

-- DROP TABLE vp.transaction;

CREATE TABLE IF NOT EXISTS vp.transaction
(
    id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    from_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    from_user_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    from_guild_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    to_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    to_user_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    to_guild_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    amount bigint NOT NULL,
    reason character varying(64) COLLATE pg_catalog."default" NOT NULL,
    middleman_id character varying(64) COLLATE pg_catalog."default" NOT NULL,
    middleman_name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    date bigint NOT NULL,
    CONSTRAINT transaction_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE vp.transaction
    OWNER to voiceprofiles;

--
-- Name: textprofile; Type: TABLE; Schema: vp; Owner: voiceprofiles
--

CREATE TABLE vp.textprofile (
    voiceprofile_id character varying(64) NOT NULL,
    level bigint NOT NULL,
    experience bigint NOT NULL,
    message_count bigint NOT NULL
);


ALTER TABLE vp.textprofile OWNER TO voiceprofiles;

--
-- Name: textprofile textprofile_pkey; Type: CONSTRAINT; Schema: vp; Owner: voiceprofiles
--

ALTER TABLE ONLY vp.textprofile
    ADD CONSTRAINT textprofile_pkey PRIMARY KEY (voiceprofile_id);


--
-- Name: textprofile voiceprofile_id; Type: FK CONSTRAINT; Schema: vp; Owner: voiceprofiles
--

ALTER TABLE ONLY vp.textprofile
    ADD CONSTRAINT voiceprofile_id FOREIGN KEY (voiceprofile_id) REFERENCES vp.voiceprofile(id);