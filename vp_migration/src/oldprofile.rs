use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use vp_core::model::profile::VoiceProfile;
use vp_core::db::hash_user_guild;
use vp_core::model::pray::Pray;
use vp_core::model::timespent::TimeSpent;
use serde_json::{Value, Number};
use vp_core::model::textprofile::TextProfile;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct OldProfile {
    pub user_id: String,
    pub guild_id: String,
    pub experience: Option<i64>,
    pub level: Option<i64>,
    pub pray_date: Value,
    pub pray_streak: Option<i64>,
    pub pray_total: Option<i64>,
    pub time_spents: HashMap<String, i64>,
    pub voicepoints: Option<i64>,
    pub data: Option<HashMap<String, Value>>,
    pub text_level: Option<i64>,
    pub text_experience: Option<i64>,
    pub text_message_count: Option<i64>,
}

impl Into<VoiceProfile> for &OldProfile {
    fn into(self) -> VoiceProfile {
        let vp_id = hash_user_guild(&self.user_id, &self.guild_id);

        let date = {
            match &self.pray_date {
                Value::Number(num) => {
                    num.as_u64().unwrap_or(0)
                }
                _ => {
                    0
                }
            }
        };

        VoiceProfile {
            god: None,
            id: vp_id.to_string(),
            user_id: self.user_id.to_string(),
            guild_id: self.guild_id.to_string(),
            pray: Some(Pray {
                voiceprofile_id: vp_id.to_string(),
                date,
                streak: self.pray_streak.unwrap_or(0),
                total: self.pray_total.unwrap_or(self.pray_streak.unwrap_or(0)),
            }),
            text: if let Some(level) = self.text_level {
                Some(TextProfile {
                    voiceprofile_id: vp_id.to_string(),
                    level,
                    experience: self.text_experience.unwrap_or(0),
                    message_count: self.text_message_count.unwrap_or(0)
                })
            } else {
                None
            },
            timespent: self.time_spents.iter().map(|(x, y)| (x.to_string(), TimeSpent(*y))).collect(),
            experience: self.experience.unwrap_or(0),
            voicepoints: self.voicepoints.unwrap_or(0),
            level: self.level.unwrap_or(1),
            data: self.data.clone()
        }
    }
}

impl From<&VoiceProfile> for OldProfile {
    fn from(profile: &VoiceProfile) -> Self {
        let mut date = Value::Null;
        let mut streak = None;
        let mut total = None;

        if let Some(pray) = &profile.pray {
            date = Value::Number(Number::from(pray.date));
            streak = Some(pray.streak);
            total = Some(pray.total);
        }

        let mut text_level = None;
        let mut text_experience = None;
        let mut text_message_count = None;

        if let Some(text) = &profile.text {
            text_level = Some(text.level as i64);
            text_experience = Some(text.experience as i64);
            text_message_count = Some(text.message_count as i64);
        }

        Self {
            user_id: profile.user_id.to_string(),
            guild_id: profile.guild_id.to_string(),
            experience: Some(profile.experience),
            level: Some(profile.level),
            pray_date: date,
            pray_streak: streak,
            pray_total: total,
            time_spents: profile.timespent.iter().map(|(x, y)| (x.to_string(), y.0)).collect(),
            voicepoints: Some(profile.voicepoints),
            data: profile.data.clone(),
            text_level,
            text_experience,
            text_message_count
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TextFromJSON {
    pub user_id: String,
    pub guild_id: String,
    pub level: i64,
    pub experience: i64,
    pub message_count: i64,
}