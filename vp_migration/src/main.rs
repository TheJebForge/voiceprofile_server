mod oldprofile;

use crate::oldprofile::{OldProfile, TextFromJSON};
use flexi_logger::{colored_opt_format, Duplicate, LevelFilter, LogSpecification, Logger};
use std::fs;
use std::sync::Arc;
use vp_core::config::Config;
use vp_core::db::DB;
use vp_core::model::pray::PrayAPI;
use vp_core::model::profile::{VoiceProfile, VoiceProfileAPI};
use vp_core::model::textprofile::TextProfileAPI;
use vp_core::model::transaction::Transaction;

#[tokio::main]
async fn main() {
    // Logger
    let mut builder = LogSpecification::builder();
    builder
        .default(LevelFilter::Off)
        .module("rocket", LevelFilter::Debug)
        .module("vp_core", LevelFilter::Trace)
        .module("vp_server", LevelFilter::Trace);

    Logger::with(builder.build())
        .duplicate_to_stdout(Duplicate::All)
        .format(colored_opt_format)
        .start().unwrap();
    
    let config = Arc::new(Config::get());
    let db = DB::connect(config.clone()).await;

    let args: Vec<String> = std::env::args().collect();

    if let Some(command) = args.get(1) {
        match command.as_str() {
            "drop" => {
                if let Some(confirm) = args.get(2) {
                    if confirm == "yes" {
                        println!("Dropping databases");
                        db.clear_database().await;
                    } else {
                        println!("To drop databases, add 'yes'");
                    }
                } else {
                    println!("To drop databases, add 'yes'");
                }

            }

            "patch" => {
                if let Some(patch) = args.get(2) {
                    match patch.as_str() {
                        "total_pray" => {
                            let profiles = db.get_profiles().await;

                            let mut counter = 0;

                            for profile in profiles {
                                if let Some(mut pray) = profile.pray {
                                    pray.total += pray.streak;

                                    println!("Patching {}", profile.user_id);
                                    counter += 1;

                                    db.update_profile(&profile.guild_id, &profile.user_id, false, VoiceProfileAPI {

                                        user_id: None,
                                        guild_id: None,
                                        pray: Some(PrayAPI {

                                            date: None,
                                            streak: None,
                                            total: Some(pray.total)
                                        }),
                                        text: None,
                                        timespent: None,
                                        experience: None,
                                        voicepoints: None,
                                        level: None,
                                        data: None,
                                    }).await.ok();
                                }
                            }

                            println!("Patched {} profiles", counter);
                        }
                        _ => println!("Specify patch"),
                    }
                }
            }

            "import" => {
                if let Some(ty) = args.get(2) {
                    if let Some(path) = args.get(3) {
                        let file_str = fs::read_to_string(path).unwrap();

                        match ty.as_str() {
                            "voiceprofile" => {
                                let profiles: Vec<VoiceProfile> = serde_json::from_str(&file_str).unwrap();

                                let amount = profiles.len();

                                for profile in profiles {
                                    println!("Importing {} with result {}", profile.id, db.create_voice_profile(&profile).await);
                                }

                                println!("Imported {} profiles", amount);
                            }
                            
                            "voiceprofile_old" => {
                                let profiles: Vec<OldProfile> = serde_json::from_str(&file_str).unwrap();

                                let modded_profiles: Vec<VoiceProfile> = profiles.iter().map(|x| x.into()).collect();

                                let amount = modded_profiles.len();

                                for profile in modded_profiles {
                                    let pray = profile.pray.unwrap();

                                    db.create_profile(&profile.guild_id, &profile.user_id, VoiceProfileAPI {
                                        user_id: Some(profile.user_id.to_string()),
                                        guild_id: Some(profile.guild_id.to_string()),
                                        pray: Some(PrayAPI {
                                            date: Some(pray.date),
                                            streak: Some(pray.streak),
                                            total: Some(pray.total)
                                        }),
                                        text: if let Some(text) = profile.text {
                                            Some(TextProfileAPI {
                                                level: Some(text.level),
                                                experience: Some(text.experience),
                                                message_count: Some(text.message_count)
                                            })
                                        } else {
                                            None
                                        },
                                        timespent: Some(profile.timespent.iter().map(|(x, y)| (x.to_string(), y.0)).collect()),
                                        experience: Some(profile.experience),
                                        voicepoints: Some(profile.voicepoints),
                                        level: Some(profile.level),
                                        data: None
                                    }).await.ok();
                                }

                                println!("Imported {} profiles", amount);
                            }

                            "textprofile" => {
                                let text_profiles: Vec<TextFromJSON> = serde_json::from_str(&file_str).unwrap();

                                let mut amount = 0;

                                for text_profile in text_profiles {
                                    if let Some(_) = db.get_profile(&text_profile.guild_id, &text_profile.user_id).await {
                                        let mut profile_update = VoiceProfileAPI::default();

                                        profile_update.text = Some(TextProfileAPI {
                                            level: Some(text_profile.level),
                                            experience: Some(text_profile.experience),
                                            message_count: Some(text_profile.message_count)
                                        });

                                        db.update_profile(&text_profile.guild_id, &text_profile.user_id, false, profile_update).await.ok();

                                        amount += 1;
                                    }
                                }

                                println!("Imported {} text profiles", amount);
                            }

                            "transaction" => {
                                let transactions: Vec<Transaction> = serde_json::from_str(&file_str).unwrap();

                                let amount = transactions.len();

                                for transaction in transactions {
                                    db.insert_transaction(transaction).await;
                                }

                                println!("Imported {} transactions", amount);
                            }

                            _ => println!("Unknown type"),
                        }
                    } else {
                        println!("Input valid path for argument #3");
                    }
                } else {
                    println!("Specify type: voiceprofile, transaction")
                }
            }

            "export" => {
                if let Some(ty) = args.get(2) {
                    match ty.as_str() {
                        "voiceprofile" => {
                            if let Some(path) = args.get(3) {
                                let profiles: Vec<VoiceProfile> = db.get_profiles().await;

                                fs::write(path, serde_json::to_string(&profiles).unwrap()).unwrap();
                                println!("Exported into '{}'", path);
                            } else {
                                println!("Input valid path for argument #3");
                            }
                        }
                        
                        "voiceprofile_old" => {
                            if let Some(path) = args.get(3) {
                                let profiles: Vec<OldProfile> = db.get_profiles().await.iter().map(|x| OldProfile::from(x)).collect();

                                fs::write(path, serde_json::to_string(&profiles).unwrap()).unwrap();
                                println!("Exported into '{}'", path);
                            } else {
                                println!("Input valid path for argument #3");
                            }
                        }

                        "transaction" => {
                            if let Some(path) = args.get(3) {
                                let transactions = db.get_transactions(None, None, None, None, None, None, None, None).await;

                                fs::write(path, serde_json::to_string(&transactions).unwrap()).unwrap();
                                println!("Exported into '{}'", path);
                            } else {
                                println!("Input valid path for argument #3");
                            }
                        }

                        _ => println!("Unknown type"),
                    }
                } else {
                    println!("Specify type: voiceprofile, transaction")
                }
            }

            _ => {
                println!("Unknown command");
            }
        }
    } else {
        println!("Provide command: drop, patch, import or export");
    }


}
