use std::sync::Arc;
use tokio::sync::RwLock;
use crate::config::Config;
use crate::db::DB;

pub struct DBManager {
    client: RwLock<Arc<DB>>
}

impl DBManager {
    pub async fn new() -> DBManager {
        DBManager {
            client: RwLock::new(DB::connect(Arc::new(Config::get())).await)
        }
    }

    pub async fn client_dead(&self) -> bool {
        self.client.read().await.check_if_dead().await
    }

    pub async fn client(&self) -> Arc<DB> {
        self.check_connection().await;
        self.client.read().await.clone()
    }

    async fn check_connection(&self) {
        if self.client_dead().await {
            self.restart_connection().await
        }
    }

    async fn restart_connection(&self) {
        log::error!("Connection closed, trying to reconnect...");

        let db = DB::connect(Arc::new(Config::get())).await;
        *self.client.write().await = db;
    }
}