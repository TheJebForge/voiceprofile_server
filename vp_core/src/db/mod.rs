pub mod manager;

use std::sync::Arc;
use crate::config::Config;
use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH};
use crate::model::profile::{VoiceProfile, VoiceProfileAPI, VoiceProfileAPIBulk};
use crate::model::pray::Pray;
use crate::model::timespent::TimeSpent;
use sha256::digest;
use tokio::sync::{Mutex, MutexGuard};
use crate::model::transaction::{Transaction, TransactionAPI};
use tokio_postgres::{NoTls, Client};
use crate::model::textprofile::TextProfile;
use serde::{Serialize, Deserialize};

#[allow(dead_code)]
pub struct DB {
    config: Arc<Config>,
    client: Arc<Mutex<Client>>,
    key_cache: Mutex<HashMap<String, String>>,
}

#[allow(dead_code)]
impl DB {
    pub async fn connect(config: Arc<Config>) -> Arc<DB> {
        let (client, connection) =
            tokio_postgres::connect(&format!("host={} port={} dbname={} user={} password={}", config.db_host, config.db_port, config.db_name, config.db_user, config.db_pass), NoTls).await.unwrap();

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                eprintln!("connection error: {}", e);
            }
        });

        Arc::new(DB {
            config,
            client: Arc::new(Mutex::new(client)),
            key_cache: Mutex::new(HashMap::new())
        })
    }

    // Util methods
    async fn get_client(&self) -> MutexGuard<'_, Client> {
        self.client.lock().await
    }

    async fn get_pray_specific(&self, client: &MutexGuard<'_, Client>, vp_id: &str) -> Option<Pray> {
        let rows = client
            .query("select * from vp.pray where voiceprofile_id = $1;", &[&vp_id])
            .await;

        match rows {
            Ok(rows) => {
                if let Some(row) = rows.get(0) {
                    Some(Pray::from(row))
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to query pray for {}: {:?}", vp_id, error);
                None
            }
        }
    }

    async fn get_pray(&self) -> HashMap<String, Pray> {
        let mut pray_map = HashMap::new();

        let client = self.get_client().await;

        let rows = client.query(r#"
            select * from vp.pray;
        "#, &[]).await;

        match rows {
            Ok(rows) => {
                let prays = rows.iter().map(|x| Pray::from(x)).collect::<Vec<Pray>>();

                for pray in prays {
                    pray_map.insert(pray.voiceprofile_id.to_string(), pray);
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error)
            }
        }

        pray_map
    }

    async fn get_text_specific(&self, client: &MutexGuard<'_, Client>, vp_id: &str) -> Option<TextProfile> {
        let rows = client
            .query("select * from vp.textprofile where voiceprofile_id = $1;", &[&vp_id])
            .await;

        match rows {
            Ok(rows) => {
                if let Some(row) = rows.get(0) {
                    Some(TextProfile::from(row))
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to query pray for {}: {:?}", vp_id, error);
                None
            }
        }
    }

    async fn get_text(&self) -> HashMap<String, TextProfile> {
        let mut text_map = HashMap::new();

        let client = self.get_client().await;

        let rows = client.query(r#"
            select * from vp.textprofile;
        "#, &[]).await;

        match rows {
            Ok(rows) => {
                let texts = rows.iter().map(|x| TextProfile::from(x)).collect::<Vec<TextProfile>>();

                for text in texts {
                    text_map.insert(text.voiceprofile_id.to_string(), text);
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error)
            }
        }

        text_map
    }

    async fn get_timespent_specific(&self, client: &MutexGuard<'_, Client>, vp_id: &str) -> HashMap<String, TimeSpent> {
        let mut map = HashMap::new();

        let rows = client
            .query("select * from vp.timespent where voiceprofile_id = $1;", &[&vp_id])
            .await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    let place: String = row.get("place");

                    map.insert(place, TimeSpent::from(&row));
                }
            }
            Err(error) => {
                log::error!("Failed to query pray for {}: {:?}", vp_id, error);
            }
        }

        map
    }

    async fn get_timespent(&self) -> HashMap<String, HashMap<String, TimeSpent>> {
        let mut timespent_map: HashMap<String, HashMap<String, TimeSpent>> = HashMap::new();

        let client = self.get_client().await;

        let rows = client.query(r#"
            select * from vp.timespent;
        "#, &[]).await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    let vp_id: &str = row.get("voiceprofile_id");
                    let place = row.get("place");

                    if let Some(timespents) = timespent_map.get_mut(vp_id) {
                        timespents.insert(place, TimeSpent::from(&row));
                    } else {
                        let mut map = HashMap::new();

                        map.insert(place, TimeSpent::from(&row));

                        timespent_map.insert(vp_id.to_string(), map);
                    }
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error)
            }
        }

        timespent_map
    }

    async fn get_god(&self) -> Vec<String> {
        let mut gods = vec![];

        let client = self.get_client().await;

        let rows = client.query(r#"
            select * from vp.godprofile;
        "#, &[]).await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    gods.push(row.get("voiceprofile_id"));
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error)
            }
        }

        gods
    }

    async fn get_god_specific(&self, client: &MutexGuard<'_, Client>, vp_id: &str) -> bool {
        let rows = client
            .query("select * from vp.godprofile where voiceprofile_id = $1;", &[&vp_id])
            .await;

        match rows {
            Ok(rows) => {
                if rows.len() > 0 {
                    true
                } else {
                    false
                }
            }
            Err(error) => {
                log::error!("Failed to query pray for {}: {:?}", vp_id, error);
                false
            }
        }
    }

    async fn get_transaction_direct(&self) -> Vec<Transaction> {
        let client = self.get_client().await;

        let rows = client.query(r#"
            select * from vp.transaction;
        "#, &[]).await;

        match rows {
            Ok(rows) => {
                let mut transactions: Vec<Transaction> = rows.iter().map(|x| Transaction::from(x)).collect();

                transactions.sort_by(|a, b| {
                    b.date.partial_cmp(&a.date).unwrap()
                });

                transactions
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error);
                vec![]
            }
        }
    }

    async fn validate_key(&self, key: &str) -> bool {
        let map = self.key_cache.lock().await;
        map.contains_key(key)
    }

    async fn update_key_cache(&self) {
        let client = self.get_client().await;
        let mut map = self.key_cache.lock().await;

        map.clear();

        let rows = client.query(r#"
            select * from server.auth;
        "#, &[]).await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    let key = row.get("key");
                    let name = row.get("name");

                    map.insert(key, name);
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error)
            }
        }
    }

    // Public methods
    pub async fn check_if_dead(&self) -> bool {
        self.client.lock().await.is_closed()
    }

    pub async fn check_key(&self, key: &str) -> Option<String> {
        let key_found = {
            if !self.validate_key(key).await {
                self.update_key_cache().await;
                self.validate_key(key).await
            } else {
                true
            }
        };

        if key_found {
            let map = self.key_cache.lock().await;
            Some(map.get(key).unwrap().to_string())
        } else {
            None
        }
    }

    pub async fn check_key_directly(&self, key: &str) -> Option<String> {
        let client = self.get_client().await;

        let rows = client.query(r#"
            select * from server.auth where key = $1;
        "#, &[&key]).await;

        match rows {
            Ok(rows) => {
                if let Some(row) = rows.get(0) {
                    Some(row.get("name"))
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profiles: {:?}", error);
                None
            }
        }
    }

    pub async fn get_profiles(&self) -> Vec<VoiceProfile> {
        let mut texts = self.get_text().await;
        let mut prays = self.get_pray().await;
        let timespents = self.get_timespent().await;

        let client = self.get_client().await;
        let rows = client.query("select * from vp.voiceprofile;", &[]).await;

        match rows {
            Ok(rows) => {
                rows.iter().map(|x| {
                    let mut profile = VoiceProfile::from(x);

                    profile.pray = prays.remove(&profile.id);
                    profile.text = texts.remove(&profile.id);

                    if let Some(timespent) = timespents.get(&profile.id) {
                        profile.timespent = timespent.clone()
                    }

                    profile
                }).collect()
            }
            Err(error) => {
                log::error!("Failed to query voice profile: {}", error);
                vec![]
            }
        }
    }

    pub async fn get_group_profiles(&self, guild_id: &str) -> Option<Vec<VoiceProfile>> {
        let mut texts = self.get_text().await;
        let mut prays = self.get_pray().await;
        let timespents = self.get_timespent().await;

        let client = self.get_client().await;
        let rows = client
            .query("select * from vp.voiceprofile where guild_id = $1;", &[&guild_id])
            .await;

        match rows {
            Ok(rows) => {
                if rows.len() > 0 {
                    Some(rows.iter().map(|x| {
                        let mut profile = VoiceProfile::from(x);

                        profile.pray = prays.remove(&profile.id);
                        profile.text = texts.remove(&profile.id);

                        if let Some(timespent) = timespents.get(&profile.id) {
                            profile.timespent = timespent.clone()
                        }

                        profile
                    }).collect())
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profile: {}", error);
                None
            }
        }
    }

    pub async fn get_guilds(&self) -> Vec<String> {
        let client = self.get_client().await;
        let rows = client
            .query("select distinct guild_id from vp.voiceprofile;", &[])
            .await;

        match rows {
            Ok(rows) => {
                rows.iter().map(|x| x.get("guild_id")).collect()
            }
            Err(error) => {
                log::error!("Failed to query voice profile: {}", error);
                vec![]
            }
        }
    }

    pub async fn get_user_guilds(&self, user_id: &str) -> Option<Vec<String>> {
        let client = self.get_client().await;
        let rows = client
            .query("select distinct guild_id from vp.voiceprofile where user_id = $1;", &[&user_id])
            .await;

        match rows {
            Ok(rows) => {
                if rows.len() > 0 {
                    Some(rows.iter().map(|x| x.get("guild_id")).collect())
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profile: {}", error);
                None
            }
        }
    }

    pub async fn get_profile(&self, guild_id: &str, user_id: &str) -> Option<VoiceProfile> {
        self.get_profile_query(guild_id, user_id).await
    }

    pub async fn get_profile_query(&self, guild_id: &str, user_id: &str) -> Option<VoiceProfile> {
        self.get_profile_by_id(&hash_user_guild(user_id, guild_id)).await
    }

    pub async fn get_profile_query_with_client(&self, client: &MutexGuard<'_, Client>, guild_id: &str, user_id: &str) -> Option<VoiceProfile> {
        self.get_profile_by_id_with_client(client, &hash_user_guild(user_id, guild_id)).await
    }

    pub async fn get_profile_by_id(&self, vp_id: &str) -> Option<VoiceProfile> {
        self.get_profile_by_id_with_client(&mut self.client.lock().await, vp_id).await
    }
    
    pub async fn get_profile_by_id_with_client(&self, client: &MutexGuard<'_, Client>, vp_id: &str) -> Option<VoiceProfile> {
        let rows = client
            .query("select * from vp.voiceprofile where id = $1;", &[&vp_id])
            .await;

        match rows {
            Ok(rows) => {
                if let Some(row) = rows.get(0) {
                    let mut voiceprofile = VoiceProfile::from(row);

                    voiceprofile.pray = self.get_pray_specific(client, &voiceprofile.id).await;
                    voiceprofile.text = self.get_text_specific(client, &voiceprofile.id).await;
                    voiceprofile.timespent = self.get_timespent_specific(client, &voiceprofile.id).await;

                    if self.get_god_specific(client, &voiceprofile.id).await {
                        voiceprofile.god = Some(());
                    }

                    Some(voiceprofile)
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to query voice profile: {}", error);
                None
            }
        }
    }

    pub async fn create_profile(&self, guild_id: &str, user_id: &str, profile: VoiceProfileAPI) -> Result<VoiceProfile, ()> {
        let mut client = self.get_client().await;
        self.create_profile_with_client(&mut client, guild_id, user_id, profile).await
    }

    async fn create_profile_with_client(&self, client: &mut MutexGuard<'_, Client>, guild_id: &str, user_id: &str, profile: VoiceProfileAPI) -> Result<VoiceProfile, ()> {
        let voiceprofile = profile.to_voiceprofile(user_id, guild_id);

        // Execute SQL
        let Ok(transaction) = client.transaction().await else {
            log::error!("Failed to create voiceprofile: failed to create transaction");
            return Err(());
        };

        match voiceprofile.create_query(&transaction).await {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to create voiceprofile: failed to create insert queries: {}", e);
                return Err(());
            }
        }
        
        let result = transaction.commit().await;

        match result {
            Ok(_) => Ok(voiceprofile),
            Err(error) => {
                log::error!("Failed to execute SQL query: {}", error);
                Err(())
            }
        }
    }

    pub async fn create_voice_profile(&self, voiceprofile: &VoiceProfile) -> bool {
        let mut client = self.get_client().await;
        
        // Execute SQL
        let Ok(transaction) = client.transaction().await else {
            log::error!("Failed to create voiceprofile: failed to create transaction");
            return false;
        };
        
        match voiceprofile.create_query(&transaction).await {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to create voiceprofile: failed to create insert queries: {}", e);
                return false;
            }
        }

        let result = transaction.commit().await;

        match result {
            Ok(_) => true,
            Err(error) => {
                log::error!("Failed to execute SQL query: {}", error);
                false
            }
        }
    }

    pub async fn update_profile(&self, guild_id: &str, user_id: &str, add: bool, profile: VoiceProfileAPI) -> Result<VoiceProfile, ()> {
        let voiceprofile = self.get_profile_query(guild_id, user_id).await;
        
        let mut client = self.get_client().await;

        if let Some(mut voiceprofile) = voiceprofile {
            let Ok(transaction) = client.transaction().await else {
                log::error!("Failed to update voiceprofile: failed to create transaction");
                return Err(());
            };

            match profile.update_query(&transaction, user_id, guild_id, add, &mut voiceprofile).await {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Failed to update voiceprofile: failed to run queries: {}", e);
                    return Err(());
                }
            }
            
            // Execute SQL
            let result = transaction.commit().await;

            match result {
                Ok(_) => {
                    Ok(if add {voiceprofile + profile} else {voiceprofile})
                },
                Err(error) => {
                    log::error!("Failed to execute SQL query: {}", error);
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }
    
    async fn collect_profiles(&self, client: &MutexGuard<'_, Client>, profiles: Vec<VoiceProfileAPIBulk>) -> Vec<(VoiceProfileAPIBulk, VoiceProfile)> {
        let mut result = vec![];
        
        for profile in profiles {
            if let Some(original_profile) = self.get_profile_by_id_with_client(
                &client, 
                &hash_user_guild(&profile.user_id, &profile.guild_id)
            ).await {
                result.push((profile, original_profile));
            }
        }
        
        result
    }

    pub async fn update_profiles(&self, add: bool, profiles: Vec<VoiceProfileAPIBulk>) -> Result<Vec<VoiceProfile>, ()> {
        let mut client = self.get_client().await;
        
        let profiles = self.collect_profiles(&client, profiles).await;
        let profile_hashes: Vec<String> = profiles.iter()
            .map(|(profile, _)| hash_user_guild(&profile.user_id, &profile.guild_id))
            .collect();

        let Ok(transaction) = client.transaction().await else {
            log::error!("Failed to update voiceprofile: failed to create transaction");
            return Err(());
        };

        for (profile, mut original_profile) in profiles {
            match profile.to_api().update_query_existing(&transaction, &mut original_profile, add).await {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Failed to update voiceprofile: failed to run queries: {}", e);
                    return Err(());
                }
            }
        }

        // Execute SQL
        let result = transaction.commit().await;

        let mut result_profiles = vec![];

        for profile_hash in profile_hashes {
            if let Some(ug_profile) = self.get_profile_by_id_with_client(&client, &profile_hash).await {
                result_profiles.push(
                    ug_profile.clone()
                )
            }
        }

        match result {
            Ok(_) => {
                Ok(result_profiles)
            },
            Err(error) => {
                log::error!("Failed to execute SQL query: {}", error);
                Err(())
            }
        }
    }

    pub async fn get_transactions(&self, user_id: Option<String>, guild_id: Option<String>,
                                  vp_id: Option<String>, reason: Option<String>,
                                  from: Option<String>, to: Option<String>,
                                  after: Option<u64>, before: Option<u64>, 
                                  limit: Option<i64>, offset: Option<i64>) -> Vec<Transaction> {
        let client = self.get_client().await;
 
        let rows = client.query(r#"
                select * from vp.transaction
                where
                    ($1::text is null or from_user_id = $1 or to_user_id = $1)
                    and ($2::text is null or from_guild_id = $2 or to_guild_id = $2)
                    and ($3::text is null or from_id = $3 or to_id = $3)
                    and ($4::text is null or reason like $4)
                    and ($5::text is null or from_user_id = $5 or from_guild_id = $5 or from_id = $5)
                    and ($6::text is null or to_user_id = $6 or to_guild_id = $6 or to_id = $6)
                    and ($7::text is null or date >= cast($7 as bigint))
                    and ($8::text is null or date <= cast($8 as bigint))            
                order by date desc
                limit $9 offset $10;             
            "#, &[
            &user_id,
            &guild_id,
            &vp_id,
            &reason,
            &from,
            &to,
            &after.map(|x| x.to_string()),
            &before.map(|x| x.to_string()),
            &limit,
            &offset
        ]).await;

        match rows {
            Ok(rows) => {
                rows.iter().map(|x| Transaction::from(x)).collect()
            }
            Err(error) => {
                log::error!("Failed to query transactions: {}", error);
                vec![]
            }
        }
    }

    pub async fn create_transaction(&self, from: &VoiceProfile, to: &VoiceProfile, middleman_name: &str, transaction_request: TransactionAPI) -> Result<Transaction, ()> {
        let mut client = self.get_client().await;

        // Creating middleman if there's none
        if let None = self.get_profile_query_with_client(&client, middleman_name, middleman_name).await {
            self.create_profile_with_client(&mut client, middleman_name, middleman_name, VoiceProfileAPI::default()).await.ok();
        }

        let transaction = transaction_request.to_transaction(from, to, middleman_name);

        let Ok(sql_transaction) = client.transaction().await else {
            log::error!("Failed to create transaction: failed to create transaction");
            return Err(());
        };

        match transaction.query(&sql_transaction, false).await {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to create transaction: failed to run queries: {}", e);
                return Err(());
            }
        }

        // Execute SQL
        let result = sql_transaction.commit().await;

        match result {
            Ok(_) => Ok(transaction),
            Err(error) => {
                log::error!("Failed to execute SQL query: {}", error);
                Err(())
            }
        }
    }

    pub async fn insert_transaction(&self, transaction: Transaction) {
        let mut client = self.get_client().await;

        let Ok(sql_transaction) = client.transaction().await else {
            log::error!("Failed to create transaction: failed to create transaction");
            return;
        };

        match transaction.query(&sql_transaction, true).await {
            Ok(_) => {}
            Err(e) => {
                log::error!("Failed to create transaction: failed to run queries: {}", e);
                return;
            }
        }

        sql_transaction.commit().await.ok();
    }

    pub async fn clear_database(&self) {
        let query = r#"
        delete from vp.pray;
        delete from vp.timespent;
        delete from vp.voiceprofile;
        delete from vp.transaction;
        "#;

        let client = self.get_client().await;

        client.batch_execute(query).await.ok();
    }
}


// Utils
pub fn hash_user_guild(user_id: &str, guild_id: &str) -> String {
    digest(format!("{}{}", user_id, guild_id))
}

pub fn build_update(table: &str, fields: &[&str], add: bool, condition: &str) -> String {
    if fields.is_empty() {
        return "".to_string();
    }
    
    let mut query = format!("update {} set ", table);

    let mut sets = vec![];
    for (index, field) in fields.iter().enumerate() {
        if add {
            sets.push(format!("{} = {} + coalesce(${}::int8, 0)", field, field, index + 1));
        } else {
            sets.push(format!("{} = coalesce(${}, {})", field, index + 1, field));
        }
    }

    query.push_str(&format!("{} where {};\n", sets.join(", "), condition));
    
    query
}

#[derive(Serialize, Deserialize)]
pub struct Statistics {
    pub total_profiles: u64,
    pub total_voicepoints_changes_last_72h: HashMap<String, BalanceChange>,
    pub amount_of_transactions_last_72h: u64,
    pub recent_transactions: Vec<Transaction>,
}

#[derive(Serialize, Deserialize)]
pub struct BalanceChange {
    pub balance: String,
    pub associated_transaction: Option<Transaction>
}

pub async fn calculate_total_balance_changes(db: Arc<DB>, guild_id: Option<String>, user_id: Option<String>, from: Option<u64>, to: Option<u64>) -> HashMap<String, BalanceChange> {
    let mut profiles = db.get_profiles().await;
    let mut transactions = db.get_transactions(None, guild_id.clone(), None, None, None, None, from, None, None, None).await;

    if let Some(guild_id) = guild_id.clone() {
        profiles.retain(|p| p.guild_id == guild_id);
        transactions.retain(|t| t.from.guild_id == guild_id || t.to.guild_id == guild_id);
    }

    let mut current_balance = 0;

    if let Some(user_id) = user_id.clone() {
        if let Some(guild_id) = guild_id.clone() {
            if let Some(profile) = db.get_profile(&guild_id, &user_id).await {
                current_balance = profile.voicepoints;
            }
        }
    } else {
        for profile in &profiles {
            current_balance += profile.voicepoints;
        }
    }

    let mut balance = HashMap::new();

    balance.insert(
        SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs(),
        BalanceChange {
            balance: current_balance.to_string(),
            associated_transaction: None
        }
    );

    for transaction in &transactions {
        if let Some(user_id) = user_id.clone() {
            if !(transaction.from.user_id == user_id || transaction.to.user_id == user_id) {
                continue;
            }
        }

        if transaction.from.id == transaction.middleman.id {
            // Bot created money
            current_balance -= transaction.amount as i64;
        } else if transaction.to.id == transaction.middleman.id {
            // Bot deleted money
            current_balance += transaction.amount as i64;
        } else if let Some(guild_id) = guild_id.clone() {
            if transaction.from.guild_id != transaction.to.guild_id {
                if transaction.from.guild_id == guild_id {
                    // Money was transferred away
                    current_balance += transaction.amount as i64;
                }

                if transaction.to.guild_id == guild_id {
                    // Money was transferred to
                    current_balance -= transaction.amount as i64;
                }
            }
        }

        balance.insert(transaction.date, BalanceChange {
            balance: current_balance.to_string(),
            associated_transaction: Some(transaction.clone())
        });
    }

    if let Some(to) = to {
        balance.retain(|t, _| t < &to);
    }

    balance.into_iter()
        .map(|(t, b)| (t.to_string(), b))
        .collect()
}

pub async fn calculate_general_stats(db: Arc<DB>, guild_id: Option<String>) -> Statistics {
    let past_timestamp = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() - 259200;

    let mut profiles = db.get_profiles().await;
    let mut transactions = db.get_transactions(None, None, None, None, None, None, Some(past_timestamp), None, None, None).await;

    if let Some(guild_id) = guild_id.clone() {
        profiles.retain(|p| p.guild_id == guild_id);
        transactions.retain(|t| t.from.guild_id == guild_id || t.to.guild_id == guild_id);
    }

    let balance_changes = calculate_total_balance_changes(
        db.clone(),
        guild_id,
        None,
        Some(past_timestamp),
        None
    ).await;

    Statistics {
        total_profiles: profiles.len() as u64,
        total_voicepoints_changes_last_72h: balance_changes,
        amount_of_transactions_last_72h: transactions.len() as u64,
        recent_transactions: transactions.drain(0..transactions.len().min(5)).collect()
    }
}
