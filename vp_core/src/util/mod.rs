use std::collections::HashMap;
use crate::model::profile::VoiceProfile;

pub fn list_to_tree_by_users(profiles: &[VoiceProfile]) -> HashMap<String, VoiceProfile> {
    let mut map = HashMap::new();

    for profile in profiles {
        map.insert(profile.user_id.clone(), profile.clone());
    }

    map
}

pub fn list_to_tree_by_groups(profiles: &[VoiceProfile]) -> HashMap<String, HashMap<String, VoiceProfile>> {
    let mut map: HashMap<String, HashMap<String, VoiceProfile>> = HashMap::new();

    for profile in profiles {
        if let Some(users) = map.get_mut(&profile.guild_id) {
            users.insert(profile.user_id.clone(), profile.clone());
        } else {
            map.insert(profile.guild_id.clone(), {
                let mut map = HashMap::new();

                map.insert(profile.user_id.clone(), profile.clone());

                map
            });
        }
    }

    map
}