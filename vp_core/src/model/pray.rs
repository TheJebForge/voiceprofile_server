use crate::db::build_update;
use serde::Deserialize;
use serde::Serialize;
use std::ops::Add;
use tokio_postgres::{GenericClient, Row};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Pray {
    #[serde(skip)]
    pub voiceprofile_id: String,

    pub date: u64,
    pub streak: i64,
    pub total: i64,
}

impl Pray {
    pub async fn create_query(&self, client: &impl GenericClient, voice_id: &str) -> Result<(), Box<dyn std::error::Error>> {
        client.execute(
            "insert into vp.pray (voiceprofile_id, date, streak, total) values ($1, $2, $3, $4);",
            &[
                &voice_id,
                &(self.date as i64),
                &self.streak,
                &self.total,
            ]
        ).await?;
        
        Ok(())
    }
}

impl From<&Row> for Pray {
    fn from(row: &Row) -> Self {
        Self {
            voiceprofile_id: row.get("voiceprofile_id"),
            date: row.get::<&str, i64>("date") as u64,
            streak: row.get("streak"),
            total: row.get("total"),
        }
    }
}

impl Add<&PrayAPI> for &Pray {
    type Output = Pray;

    fn add(self, rhs: &PrayAPI) -> Self::Output {
        let mut out = self.clone();

        if let Some(date) = rhs.date {
            out.date += date;
        }

        if let Some(streak) = rhs.streak {
            out.streak += streak;
        }

        if let Some(total) = rhs.total {
            out.total += total;
        }

        out
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PrayAPI {
    pub date: Option<u64>,
    pub streak: Option<i64>,
    pub total: Option<i64>,
}

impl Default for PrayAPI {
    fn default() -> Self {
        PrayAPI {
            date: None,
            streak: None,
            total: None
        }
    }
}

impl PrayAPI {
    pub fn to_pray(&self, voiceprofile_id: &str) -> Pray {
        Pray {
            voiceprofile_id: voiceprofile_id.to_string(),
            date: self.date.unwrap_or(0),
            streak: self.streak.unwrap_or(0),
            total: self.total.unwrap_or(0),
        }
    }

    pub fn update_existing(&self, existing: &mut Pray) {
        if let Some(date) = self.date {
            existing.date = date;
        }

        if let Some(streak) = self.streak {
            existing.streak = streak;
        }

        if let Some(total) = self.total {
            existing.total = total;
        }
    }

    pub async fn update_query(&self, client: &impl GenericClient, vp_id: &str, add: bool) -> Result<(), Box<dyn std::error::Error>> {
        client.execute(
            &build_update(
                "vp.pray",
                &[
                    "date",
                    "streak",
                    "total"
                ],
                add,
                "voiceprofile_id = $4"
            ),
            &[
                &self.date.map(|x| x as i64),
                &self.streak,
                &self.total,
                &vp_id
            ]
        ).await?;
        
        Ok(())
    }
}