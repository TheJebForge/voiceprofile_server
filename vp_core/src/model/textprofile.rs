use crate::db::build_update;
use serde::Deserialize;
use serde::Serialize;
use std::ops::Add;
use tokio_postgres::{GenericClient, Row};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TextProfile {
    #[serde(skip)]
    pub voiceprofile_id: String,

    pub level: i64,
    pub experience: i64,
    pub message_count: i64,
}

impl TextProfile {
    pub async fn create_query(&self, client: &impl GenericClient, voice_id: &str) -> Result<(), Box<dyn std::error::Error>> {
        client.execute(
            "insert into vp.textprofile (voiceprofile_id, level, experience, message_count) values ($1, $2, $3, $4);",
            &[
                &voice_id,
                &self.level,
                &self.experience,
                &self.message_count,
            ]
        ).await?;
        
        Ok(())
    }
}

impl From<&Row> for TextProfile {
    fn from(row: &Row) -> Self {
        Self {
            voiceprofile_id: row.get("voiceprofile_id"),
            level: row.get::<&str, i64>("level"),
            experience: row.get::<&str, i64>("experience"),
            message_count: row.get::<&str, i64>("message_count")
        }
    }
}

impl Add<&TextProfileAPI> for &TextProfile {
    type Output = TextProfile;

    fn add(self, rhs: &TextProfileAPI) -> Self::Output {
        let mut out = self.clone();

        if let Some(level) = rhs.level {
            out.level += level;
        }

        if let Some(experience) = rhs.experience {
            out.experience += experience
        }

        if let Some(message_count) = rhs.message_count {
            out.message_count += message_count;
        }

        out
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TextProfileAPI {
    pub level: Option<i64>,
    pub experience: Option<i64>,
    pub message_count: Option<i64>,
}

impl Default for TextProfileAPI {
    fn default() -> Self {
        Self {
            level: None,
            experience: None,
            message_count: None
        }
    }
}

impl TextProfileAPI {
    pub fn to_text(&self, voiceprofile_id: &str) -> TextProfile {
        TextProfile {
            voiceprofile_id: voiceprofile_id.to_string(),
            level: self.level.unwrap_or(0),
            experience: self.experience.unwrap_or(0),
            message_count: self.message_count.unwrap_or(0)
        }
    }

    pub fn update_existing(&self, existing: &mut TextProfile) {
        if let Some(level) = self.level {
            existing.level = level;
        }

        if let Some(experience) = self.experience {
            existing.experience = experience;
        }

        if let Some(message_count) = self.message_count {
            existing.message_count = message_count;
        }
    }

    pub async fn update_query(&self, client: &impl GenericClient, vp_id: &str, add: bool) -> Result<(), Box<dyn std::error::Error>> {
        client.execute(
            &build_update(
                "vp.textprofile",
                &[
                    "level",
                    "experience",
                    "message_count"
                ],
                add,
                "voiceprofile_id = $4"
            ),
            &[
                &self.level,
                &self.experience,
                &self.message_count,
                &vp_id
            ]
        ).await?;
        
        Ok(())
    }
}