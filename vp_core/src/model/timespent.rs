use serde::Serialize;
use serde::Deserialize;
use tokio_postgres::{GenericClient, Row};
use std::ops::Add;

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub struct TimeSpent(pub i64);

impl TimeSpent {
    pub async fn create_query(&self, client: &impl GenericClient, vp_id: &str, place: &str) -> Result<(), Box<dyn std::error::Error>> {
        client.execute(
            "insert into vp.timespent (voiceprofile_id, place, time) values ($1, $2, $3);",
            &[
                &vp_id,
                &place,
                &self.0,
            ]
        ).await?;
        
        Ok(())
    }
}

impl From<&Row> for TimeSpent {
    fn from(row: &Row) -> Self {
        Self(row.get("time"))
    }
}

impl Add<i64> for TimeSpent {
    type Output = TimeSpent;

    fn add(self, rhs: i64) -> Self::Output {
        TimeSpent(self.0 + rhs)
    }
}