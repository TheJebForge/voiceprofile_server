use serde::Serialize;
use serde::Deserialize;
use tokio_postgres::{GenericClient, Row};
use crate::db::{build_update, hash_user_guild};
use crate::model::pray::{Pray, PrayAPI};
use crate::model::timespent::{TimeSpent};
use std::collections::HashMap;
use std::ops::Add;
use serde_json::Value;
use crate::merge;
use crate::model::textprofile::{TextProfile, TextProfileAPI};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct VoiceProfile {
    #[serde(skip)]
    pub god: Option<()>,

    pub id: String,
    pub user_id: String,
    pub guild_id: String,
    pub pray: Option<Pray>,
    pub text: Option<TextProfile>,
    pub timespent: HashMap<String, TimeSpent>,
    pub experience: i64,
    pub voicepoints: i64,
    pub level: i64,
    pub data: Option<HashMap<String, Value>>,
}

impl VoiceProfile {
    pub fn ug_hash(&self) -> String {
        hash_user_guild(&self.user_id, &self.guild_id)
    }

    pub async fn create_query(&self, client: &impl GenericClient) -> Result<(), Box<dyn std::error::Error>> {
        client.execute(r#"
            delete from vp.pray where voiceprofile_id = $1;
        "#, &[&self.id]).await?;

        client.execute(r#"
            delete from vp.textprofile where voiceprofile_id = $1;
        "#, &[&self.id]).await?;

        client.execute(r#"
            delete from vp.timespent where voiceprofile_id = $1;
        "#, &[&self.id]).await?;

        client.execute(r#"
            delete from vp.voiceprofile where id = $1;
        "#, &[&self.id]).await?;
        
        client.execute(
            "insert into vp.voiceprofile (id, user_id, guild_id, experience, voicepoints, level, data) values ($1, $2, $3, $4, $5, $6, $7);",
            &[
                &self.id,
                &self.user_id,
                &self.guild_id,
                &self.experience,
                &self.voicepoints,
                &self.level,
                &self.data.clone().map(|m| serde_json::to_string(&m).unwrap()),
            ]
        ).await?;

        if let Some(pray) = &self.pray {
            pray.create_query(client, &self.id).await?;
        }
        
        if let Some(text) = &self.text {
            text.create_query(client, &self.id).await?;
        }
        
        for (place, timespent) in &self.timespent {
            timespent.create_query(client, &self.id, place).await?;
        }

        Ok(())
    }
}

impl Add<VoiceProfileAPI> for VoiceProfile {
    type Output = VoiceProfile;

    fn add(mut self, rhs: VoiceProfileAPI) -> Self::Output {
        let mut out = self.clone();

        if let Some(voicepoints) = rhs.voicepoints {
            out.voicepoints += voicepoints;
        }

        if let Some(experience) = rhs.experience {
            out.experience += experience;
        }

        // Pray
        if let Some(lhs_pray) = &out.pray {
            if let Some(rhs_pray) = &rhs.pray {
                out.pray = Some(lhs_pray + &rhs_pray);
            }
        } else {
            if let Some(rhs_pray) = &rhs.pray {
                out.pray = Some(rhs_pray.to_pray(&out.id));
            }
        }

        // Text
        if let Some(lhs_text) = &out.text {
            if let Some(rhs_text) = &rhs.text {
                out.text = Some(lhs_text + &rhs_text);
            }
        } else {
            if let Some(rhs_text) = &rhs.text {
                out.text = Some(rhs_text.to_text(&out.id));
            }
        }

        // TimeSpent
        if let Some(rhs_timespent) = &rhs.timespent {
            for (rhs_place, rhs_value) in rhs_timespent {
                if let Some(lhs_timespent) = out.timespent.get_mut(rhs_place) {
                    let result = lhs_timespent.clone() + *rhs_value;
                    *lhs_timespent = result;
                } else {
                    out.timespent.insert(rhs_place.to_string(), TimeSpent(*rhs_value));
                }
            }
        }

        if let Some(rhs_data) = &rhs.data {
            if let Some(lhs_data) = &self.data {
                let mut lhs_value = serde_json::to_value(lhs_data).unwrap();
                let mut rhs_value = serde_json::to_value(rhs_data).unwrap();

                merge(&mut lhs_value, &mut rhs_value);

                self.data = Some(serde_json::from_value(lhs_value).unwrap());
            } else {
                self.data = Some(rhs_data.clone());
            }
        }

        out
    }
}

impl From<&Row> for VoiceProfile {
    fn from(row: &Row) -> Self {
        Self {
            god: None,
            id: row.get("id"),
            user_id: row.get("user_id"),
            guild_id: row.get("guild_id"),
            pray: None,
            text: None,
            timespent: HashMap::new(),
            experience: row.get("experience"),
            voicepoints: row.get("voicepoints"),
            level: row.get("level"),
            data: {
                if let Ok(json) = row.try_get::<&str, String>("data") {
                    let json = html_escape::decode_html_entities(&json).to_string();
                    let json = html_escape::decode_html_entities(&json).to_string();

                    if let Ok(data) = serde_json::from_str::<HashMap<String, Value>>(&json) {
                        Some(data)
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct VoiceProfileAPI {
    pub user_id: Option<String>,
    pub guild_id: Option<String>,
    pub pray: Option<PrayAPI>,
    pub text: Option<TextProfileAPI>,
    pub timespent: Option<HashMap<String, i64>>,
    pub experience: Option<i64>,
    pub voicepoints: Option<i64>,
    pub level: Option<i64>,
    pub data: Option<HashMap<String, Value>>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct VoiceProfileAPIBulk {
    pub user_id: String,
    pub guild_id: String,
    pub pray: Option<PrayAPI>,
    pub text: Option<TextProfileAPI>,
    pub timespent: Option<HashMap<String, i64>>,
    pub experience: Option<i64>,
    pub voicepoints: Option<i64>,
    pub level: Option<i64>,
    pub data: Option<HashMap<String, Value>>,
}

impl VoiceProfileAPI {
    pub fn default() -> VoiceProfileAPI {
        VoiceProfileAPI {
            user_id: None,
            guild_id: None,
            pray: Some(PrayAPI::default()),
            text: None,
            timespent: None,
            experience: None,
            voicepoints: None,
            level: None,
            data: None
        }
    }

    pub fn restify(self) -> VoiceProfileAPI {
        VoiceProfileAPI {
            user_id: self.user_id,
            guild_id: self.guild_id,
            pray: self.pray,
            text: self.text,
            timespent: self.timespent,
            experience: self.experience,
            voicepoints: None,
            level: self.level,
            data: self.data
        }
    }

    pub fn to_voiceprofile(&self, user_id: &str, guild_id: &str) -> VoiceProfile {
        let vp_id = hash_user_guild(user_id, guild_id);

        VoiceProfile {
            god: None,
            id: vp_id.to_string(),
            user_id: user_id.to_string(),
            guild_id: guild_id.to_string(),
            pray: match &self.pray {
                None => Some (
                    Pray {
                        voiceprofile_id: vp_id.to_string(),
                        date: 0,
                        streak: 0,
                        total: 0
                    }
                ),
                Some(api) => {
                    Some(api.to_pray(&vp_id))
                }
            },
            text: match &self.text {
                None => None,
                Some(api) => {
                    Some(api.to_text(&vp_id))
                }
            },
            timespent: match &self.timespent {
                None => HashMap::new(),
                Some(timespents) => {
                    let mut result = HashMap::new();

                    for (place, value) in timespents {
                        result.insert(place.clone(), TimeSpent(*value));
                    }

                    result
                }
            },
            experience: self.experience.unwrap_or(0),
            voicepoints: self.voicepoints.unwrap_or(0),
            level: self.level.unwrap_or(1),
            data: self.data.clone()
        }
    }

    pub async fn update_query(&self, client: &impl GenericClient, user_id: &str, guild_id: &str, add: bool, existing: &mut VoiceProfile) -> Result<(), Box<dyn std::error::Error>> {
        let vp_id = hash_user_guild(user_id, guild_id);
        
        if let Some(pray) = &self.pray {
            if existing.pray.is_none() {
                let new_pray = pray.to_pray(&vp_id);

                existing.pray = Some(new_pray.clone());
                new_pray.create_query(client, &vp_id).await?;
            } else {
                if !add {
                    pray.update_existing(existing.pray.as_mut().unwrap());
                }

                pray.update_query(client, &vp_id, add).await?;
            }
        }

        if let Some(text) = &self.text {
            if existing.text.is_none() {
                let new_text = text.to_text(&vp_id);

                existing.text = Some(new_text.clone());
                new_text.create_query(client, &vp_id).await?;
            } else {
                if !add {
                    text.update_existing(existing.text.as_mut().unwrap());
                }

                text.update_query(client, &vp_id, add).await?;
            }
        }

        if let Some(timespents) = &self.timespent {
            for (place, value) in timespents {
                if let Some(_) = existing.timespent.get(place) {
                    if add {
                        client.execute(
                            r#"
                                update vp.timespent 
                                set time = time + $1 
                                where voiceprofile_id = $2 and place = $3;
                            "#,
                            &[
                                &value,
                                &vp_id,
                                &place
                            ]
                        ).await?;
                    } else {
                        existing.timespent.insert(place.to_string(), TimeSpent(*value));
                        client.execute(
                            r#"
                                update vp.timespent 
                                set time = $1
                                where voiceprofile_id = $2 and place = $3;
                            "#,
                            &[
                                &value,
                                &vp_id,
                                &place
                            ]
                        ).await?;
                    }
                } else {
                    client.execute(
                        r#"
                            INSERT INTO vp.timespent(voiceprofile_id, place, time) 
                            VALUES ($1, $2, $3);
                        "#,
                        &[
                            &vp_id,
                            &place,
                            &value
                        ]
                    ).await?;
                }
            }
        }

        client.execute(
            &build_update(
                "vp.voiceprofile",
                &[
                    "experience",
                    "voicepoints",
                    "level"
                ],
                add,
                "id = $4"
            ),
            &[
                &self.experience,
                &self.voicepoints,
                &self.level,
                &vp_id
            ]
        ).await?;
        
        if let Some(experience) = &self.experience {
            if add {
                existing.experience += *experience;
            } else {
                existing.experience = *experience;
            }
        }

        if let Some(voicepoints) = &self.voicepoints {
            if add {
                existing.voicepoints += *voicepoints;
            } else {
                existing.voicepoints = *voicepoints;
            }
        }

        if let Some(level) = &self.level {
            if add {
                existing.level += *level;
            } else {
                existing.level = *level;
            }
        }

        if let Some(rhs_data) = &self.data {
            let value = if add {
                if let Some(lhs_data) = &existing.data {
                    let mut lhs_value = serde_json::to_value(lhs_data).unwrap();
                    let mut rhs_value = serde_json::to_value(rhs_data).unwrap();

                    merge(&mut lhs_value, &mut rhs_value);

                    serde_json::from_value(lhs_value).unwrap()
                } else {
                    rhs_data.clone()
                }
            } else {
                rhs_data.clone()
            };

            existing.data = Some(value.clone());

            let json_string = html_escape::encode_quoted_attribute(&serde_json::to_string(&value).unwrap()).to_string();

            client.execute(
                &build_update(
                    "vp.voiceprofile",
                    &[
                        "data"
                    ],
                    false,
                    "id = $2"
                ),
                &[
                    &json_string,
                    &vp_id
                ]
            ).await?;
        }

        Ok(())
    }

    pub async fn update_query_existing(&self, client: &impl GenericClient, profile: &mut VoiceProfile, add: bool) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(user_id) = &self.user_id {
            if let Some(guild_id) = &self.guild_id {
                self.update_query(client, user_id, guild_id, add, profile).await?;
            }
        }
        
        Ok(())
    }
}

impl VoiceProfileAPIBulk {
    pub fn to_api(&self) -> VoiceProfileAPI {
        VoiceProfileAPI {
            user_id: Some(self.user_id.to_string()),
            guild_id: Some(self.guild_id.to_string()),
            pray: self.pray.clone(),
            text: self.text.clone(),
            timespent: self.timespent.clone(),
            experience: self.experience,
            voicepoints: self.voicepoints,
            level: self.level,
            data: self.data.clone()
        }.restify()
    }
}