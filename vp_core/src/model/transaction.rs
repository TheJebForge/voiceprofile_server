use serde::Serialize;
use serde::Deserialize;
use serde_json::Value;
use crate::db::hash_user_guild;
use tokio_postgres::{GenericClient, Row};
use crate::model::profile::VoiceProfile;
use std::time::{SystemTime, UNIX_EPOCH};
use sha256::digest;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Transaction {
    pub id: String,
    pub from: Addressant,
    pub to: Addressant,
    pub amount: u64,
    pub reason: String,
    pub date: u64,
    pub middleman: Addressant
}

impl Transaction {
    pub async fn query(&self, client: &impl GenericClient, just_insert: bool) -> Result<(), Box<dyn std::error::Error>> {
        let transaction_id = self.id.to_string();
        
        client.execute(
            r#"
                INSERT INTO vp.transaction(
                    id, from_id, from_user_id, from_guild_id, to_id, to_user_id, to_guild_id, amount, reason, middleman_id, middleman_name, date
                ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);
            "#,
            &[
                &transaction_id,
                
                &self.from.id,
                &self.from.user_id,
                &self.from.guild_id,
                
                &self.to.id,
                &self.to.user_id,
                &self.to.guild_id,
                
                &(self.amount as i64),
                &self.reason,
                
                &self.middleman.id,
                &self.middleman.user_id,
                
                &(self.date as i64)
            ]
        ).await?;

        if !just_insert {
            if !self.from.god {
                client.execute(
                    r#"
                        update vp.voiceprofile
                        set voicepoints = voicepoints - $1
                        where id = $2;
                    "#,
                    &[
                        &(self.amount as i64),
                        &self.from.id
                    ]
                ).await?;
            }

            if !self.to.god {
                client.execute(
                    r#"
                        update vp.voiceprofile
                        set voicepoints = voicepoints + $1
                        where id = $2;
                    "#,
                    &[
                        &(self.amount as i64),
                        &self.to.id
                    ]
                ).await?;
            }
        }

        Ok(())
    }
}

impl From<&Row> for Transaction {
    fn from(row: &Row) -> Self {
        Self {
            id: row.get("id"),
            from: Addressant {
                god: false,

                id: row.get("from_id"),
                user_id: row.get("from_user_id"),
                guild_id: row.get("from_guild_id")
            },
            to: Addressant {
                god: false,

                id: row.get("to_id"),
                user_id: row.get("to_user_id"),
                guild_id: row.get("to_guild_id")
            },
            amount: row.get::<&str, i64>("amount") as u64,
            reason: row.get("reason"),
            date: row.get::<&str, i64>("date") as u64,
            middleman: Addressant {
                god: false,

                id: row.get("middleman_id"),
                user_id: row.get("middleman_name"),
                guild_id: row.get("middleman_name")
            }
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Addressant {
    #[serde(skip)]
    pub god: bool,

    pub id: String,
    pub user_id: String,
    pub guild_id: String,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct TransactionAPI {
    pub from: AddressantAPI,
    pub to: AddressantAPI,
    pub amount: u64,
    pub reason: String,

    #[serde(skip)]
    pub middleman_id: Option<String>
}

impl TransactionAPI {
    pub fn to_transaction(&self, from: &VoiceProfile, to: &VoiceProfile, middleman_name: &str) -> Transaction {
        let middleman_id = hash_user_guild(middleman_name, middleman_name);

        let reason = html_escape::encode_text(&self.reason).to_string();
        let date = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();

        let transaction_id = digest(format!("{}{}{}{}{}{}{}", from.user_id, from.guild_id, to.user_id, to.guild_id, reason, date, SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_nanos()));

        Transaction {
            id: transaction_id.to_string(),
            from: Addressant {
                god: from.god.is_some(),

                id: html_escape::encode_text(&from.id).to_string(),
                user_id: html_escape::encode_text(&from.user_id).to_string(),
                guild_id: html_escape::encode_text(&from.guild_id).to_string()
            },
            to: Addressant {
                god: to.god.is_some(),

                id: html_escape::encode_text(&to.id).to_string(),
                user_id: html_escape::encode_text(&to.user_id).to_string(),
                guild_id: html_escape::encode_text(&to.guild_id).to_string()
            },
            amount: self.amount,
            reason,
            date,
            middleman: Addressant {
                god: false,

                id: html_escape::encode_text(&middleman_id).to_string(),
                user_id: html_escape::encode_text(&middleman_name).to_string(),
                guild_id: html_escape::encode_text(&middleman_name).to_string()
            }
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct AddressantAPI(Value);

impl AddressantAPI {
    pub fn to_vp_id(&self, middleman_id: &String) -> Option<String> {
        match &self.0 {
            Value::String(str) => {
                if str == "self" {
                    Some(middleman_id.to_string())
                } else {
                    Some(str.to_string())
                }
            },
            Value::Object(obj) => {
                if let Some(user_id) = obj.get("user_id") {
                    let user_id = html_escape::encode_text(&user_id.as_str().unwrap_or(&user_id.to_string())).to_string();

                    if let Some(guild_id) = obj.get("guild_id") {
                        let guild_id = html_escape::encode_text(&guild_id.as_str().unwrap_or(&guild_id.to_string())).to_string();

                        Some(hash_user_guild(&user_id, &guild_id))
                    } else {
                        None
                    }
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}