use serde::Deserialize;
use std::fs;

#[derive(Deserialize)]
pub struct Config {
    pub db_host: String,
    pub db_port: String,
    pub db_name: String,
    pub db_user: String,
    pub db_pass: String,
}

impl Config {
    pub fn get() -> Config {
        toml::from_str(&fs::read_to_string("config.toml")
            .expect("Couldn't load config"))
            .expect("Missing fields in config")
    }
}