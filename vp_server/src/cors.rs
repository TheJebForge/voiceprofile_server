use std::collections::{HashMap};
use rocket::{async_trait, Build, Data, Rocket, Route};
use rocket::http::{Header, Method};
use rocket::{Request, Response};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::route::{Handler, Outcome};

pub struct CORS;

#[async_trait]
impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response
        }
    }

    async fn on_response<'r>(&self, _req: &'r Request<'_>, res: &mut Response<'r>) {
        res.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        res.set_header(Header::new("Access-Control-Allow-Methods", "POST, GET, PATCH, OPTIONS"));
        res.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        res.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}

#[derive(Clone)]
struct OptionsHandler;

#[rocket::async_trait]
impl Handler for OptionsHandler {
    async fn handle<'r>(&self, req: &'r Request<'_>, _: Data<'r>) -> Outcome<'r> {
        Outcome::from(req, "")
    }
}

pub fn auto_gen_options(rocket: Rocket<Build>) -> Rocket<Build> {
    let regex = regex::Regex::new("([^?]+)").unwrap();

    let mut unique_uris = HashMap::new();

    for route in rocket.routes() {
        if let Some(captures) = regex.captures(route.uri.as_str()) {
            unique_uris.insert(captures.get(1).unwrap().as_str(), route.rank);
        }
    }

    let mut routes = vec![];

    for (uri, rank) in unique_uris {
        log::info!("#{} {}", rank, uri);
        routes.push(Route::ranked(rank, Method::Options, uri, OptionsHandler))
    }

    rocket.mount("/", routes)
}