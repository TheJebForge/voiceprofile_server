use std::sync::Arc;
use rocket::http::{ContentType, Status};
use rocket::State;
use vp_core::db::{calculate_general_stats, calculate_total_balance_changes};
use vp_core::db::manager::DBManager;
use crate::endpoints::StatusStringResponse;

#[get("/stats?<guild_id>")]
pub async fn get_statistics(db: &State<Arc<DBManager>>, guild_id: Option<String>) -> StatusStringResponse {
    let db = db.client().await;

    let stats = calculate_general_stats(db.clone(), guild_id.clone()).await;
    (Status::Ok, (ContentType::JSON, serde_json::to_string(&stats).unwrap()))
}

#[get("/stats/balance_graph?<guild_id>&<user_id>&<from>&<to>")]
pub async fn get_balance_graph(db: &State<Arc<DBManager>>, guild_id: Option<String>, user_id: Option<String>, from: Option<u64>, to: Option<u64>) -> StatusStringResponse {
    let db = db.client().await;

    let balance_graph = calculate_total_balance_changes(db.clone(), guild_id, user_id, from, to).await;
    (Status::Ok, (ContentType::JSON, serde_json::to_string(&balance_graph).unwrap()))
}
