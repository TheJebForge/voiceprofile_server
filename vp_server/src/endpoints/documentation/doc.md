# Voice Profile Server
### Dedicated server to manage voice profiles

## Authorization:
API key will be provided individually and is required to use the REST API

### Header:
    Authorization: <KEY>

## GET /
Shows this page

# Adding data

## POST /profile/{guild_id}/{user_id}
Creates voice profile with specified guild_id and user_id.

Empty body will create a profile with 0 experience or voicepoints

#### Using this on existing profile will override it and reset it

### Optional Request Body:
None of the fields are required, defaults will be assumed instead

```json
{
  "experience": 0,
  "voicepoints": 0,
  "level": 1,
  "pray": {
    "date": 0,
    "streak": 0
  },
  "text": null,
  "timespent": {
    "global": 0
  },
  "data": null
}
```

# Fetching profiles

## GET /profile
Fetches all profiles from database
### Parameters:
    limit=<int> - amount of profiles to fetch, if amount is 0 or less, will fetch everything. default 50
    page=<int> - page offset that depends on limit, used to fetch more profiles. default 0
    view=<list/tree> - which view to present, tree view ignores page and limit parameters. default list

## GET /profile/{guild_id}
Fetches profiles from specified guild
### Parameters:
    limit=<int> - amount of profiles to fetch, if amount is 0 or less, will fetch everything. default 50
    page=<int> - page offset that depends on limit, used to fetch more profiles. default 0
    view=<list/tree> - which view to present, tree view ignores page and limit parameters. default list

## GET /profile/guilds
Returns array of all guilds that have at least 1 profile on them

## GET /profile/guilds/{user_id}
Returns array of all guild_ids that a user has a profile on

## GET /profile/{guild_id}/{user_id}
Fetches specific profile

# Editing profiles

### From now on, voicepoints cannot be directly modified. You now must execute a transaction to do so, refer to [this section](#transactions)

## PATCH /profile/
Allows bulk edit of many profiles
### Request Body:
Request body must be an array

Required fields for each array item:
- user_id
- guild_id

Missing fields will be ignored and won't be changed

```json
[
  {
    "user_id": "1242154218",
    "guild_id": "131853185391",
    "experience": 0,
    "voicepoints": 0,
    "level": 1,
    "pray": {
      "date": 0,
      "streak": 0
    },
    "text": {
      "level": 0,
      "experience": 0,
      "message_count": 0
    },
    "timespent": {
      "global": 0
    },
    "data": {
      "anything": "anywhere"
    }
  }
]
```

## PATCH /profile/add
Instead of editing, adds to existing values. Bulk adds to many profiles at once
### Request Body:
Request body must be an array

Required fields for each array item:
- user_id
- guild_id

Missing fields will be ignored and won't be changed
```json
[
    {
        "user_id": "1242154218",
        "guild_id": "131853185391",
        "experience": 0,
        "voicepoints": 0,
        "level": 1,
        "pray": {
            "date": 0,
            "streak": 0
        },
        "text": {
          "level": 0,
          "experience": 0,
          "message_count": 0
        },
        "timespent": {
            "global": 0
        },
      "data": {
        "anything": "anywhere"
      }
    }
]
```

## PATCH /profile/{guild_id}/{user_id}
Edits profile with provided body
### Request Body:
None of the fields are required, missing fields will be ignored and won't be changed
```json
{
  "experience": 0,
  "voicepoints": 0,
  "level": 1,
  "pray": {
    "date": 0,
    "streak": 0
  },
  "text": {
    "level": 0,
    "experience": 0,
    "message_count": 0
  },
  "timespent": {
    "global": 0
  },
  "data": {
    "anything": "you can put anything here"
  }
}
```

## PATCH /profile/{guild_id}/{user_id}/add
Instead of overriding the data, it adds it together
### Request Body:
None of the fields are required, missing fields will be ignored and won't be changed
```json
{
  "experience": 0,
  "voicepoints": 0,
  "level": 1,
  "pray": {
    "date": 0,
    "streak": 0
  },
  "text": {
    "level": 0,
    "experience": 0,
    "message_count": 0
  },
  "timespent": {
    "global": 0
  },
  "data": {
    "anything": "you can put anything here"
  }
}
```

<a id="transactions"><h1>Transactions</h1></a>

## GET /transaction
Gets all recent transactions
### Parameters:
    limit=<int> - amount of transactions to fetch, if amount is 0 or less, will fetch everything. default 50
    page=<int> - page offset that depends on limit, used to fetch more profiles. default 0
    user_id=<snowflake> - filters transactions to have to mention specified user_id in either 'from' or 'to' addressants
    guild_id=<snowflake> - filters transactions to have to mention specified guild_id in either 'from' or 'to' addressants
    vp_id=<hash> - filters transactions to have to mention specified voiceprofile id in either 'from' or 'to' addressants
    reason=<string> - filters transactions to have to include specified text in reason
    from=<string> - filters transactions to have to either have user_id, guild_id or vp_id to be equal in 'from' addressant
    to=<string> - filters transactions to have to either have user_id, guild_id or vp_id to be equal in 'to' addressant
    after=<seconds> - filters transactions to have to happen after specified unix timestamp
    before=<seconds> - filters transactions to have to happen before specified unix timestamp

## POST /transaction
Performs a transaction from "from" voice profile to "to" voice profile, amount cannot be lower than 1. Transaction also cannot be performed as another bot, no impostors here. Sender and receiver also cannot be the same.
### Request Body Examples:
Sends voicepoints between 2 profiles
```json
{
  "from": "4fc82b26aecb47d2868c4efbe3581732a3e7cbcc6c2efb32062c08170a05eeb8",
  "to": "10242124710dae0dfa014712041207aded30147188240127aad014712407a124",
  "amount": 300,
  "reason": "very generous gift"
}
```
Sends voicepoints from bot's voice profile to someone's voice profile
```json
{
  "from": "self",
  "to": {
    "user_id": "135315351315",
    "guild_id": "151231351535"
  },
  "amount": 300,
  "reason": "level up bonus"
}
```
Takes voicepoints from someone's profile and puts them in bot's voice profile
```json
{
  "from": "10242124710dae0dfa014712041207aded30147188240127aad014712407a124",
  "to": "self",
  "amount": 300,
  "reason": "penalty"
}
```

# Top

## GET /profile/top?by=<top_type>
Gets top based on all profiles in the database with specified sorting
### Available top types:
    points - richest profiles
    levels - most experienced profiles
    time - profiles that were in voice channels the longest
    text-level - most experienced typers
    text-message - profiles that typed the most messages
    pray-streak - profiles that currently lead in unochi prayers
### Parameters:
    by=<top_type> - which sorting to use
    amount=<int> - how much people should be in the top, <= 0 for all profiles sorted by voice points. default 5
    offset=<int> - offsets who to start with, can used to get tops in series instead of getting everything. default 0

## GET /profile/{guild_id}/top?by=<top_type>
Gets top based on profiles from specified guild with specified sorting
### Available top types:
    points - richest profiles
    levels - most experienced profiles
    time - profiles that were in voice channels the longest
    text-level - most experienced typers
    text-message - profiles that typed the most messages
    pray-streak - profiles that currently lead in unochi prayers
### Parameters:
    by=<top_type> - which sorting to use
    amount=<int> - how much people should be in the top, <= 0 for all profiles sorted by voice points. default 5
    offset=<int> - offsets who to start with, can used to get tops in series instead of getting everything. default 0

# Stats

## GET /stats
Gets statistics about voice profile system
### Parameters:
    guild_id=<snowflake> - Optionally can tell stats about a specific guild

## GET /stats/balance_graph
Gets balance changes with provided filters
### Parameters:
    guild_id=<snowflake> - filters to only show changes from specified guild
    user_id=<snowflake> - get balance graph for user, requires specifying guild_id as well
    from=<epoch seconds> - only display changes after specified timestamp
    to=<epoch seconds> - only display changes before specified timestamp


# GitLab link
You can find source of this server here:
[GitLab](https://gitlab.com/TheJebForge/voiceprofile_server)
