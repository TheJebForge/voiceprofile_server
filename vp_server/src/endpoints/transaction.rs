use std::sync::Arc;
use vp_core::db::{hash_user_guild};
use vp_core::model::transaction::{TransactionAPI};
use vp_core::model::profile::VoiceProfileAPI;
use crate::endpoints::{StatusStringResponse, Auth};
use rocket::http::{Status, ContentType};
use rocket::State;
use rocket::serde::json::Json;
use vp_core::db::manager::DBManager;

#[get("/transaction?<limit>&<page>&<user_id>&<guild_id>&<vp_id>&<reason>&<from>&<to>&<after>&<before>")]
pub async fn get_transactions(db: &State<Arc<DBManager>>, limit: Option<u32>, page: Option<i32>,
                              user_id: Option<String>, guild_id: Option<String>,
                              vp_id: Option<String>, reason: Option<String>,
                              from: Option<String>, to: Option<String>,
                              after: Option<u64>, before: Option<u64>) -> StatusStringResponse {
    let db = db.client().await;

    let limit_number = limit.unwrap_or(50);
    
    let page = page.unwrap_or(0);

    let start = limit_number * page as u32;

    let transactions = db.get_transactions(
        user_id, 
        guild_id, 
        vp_id, 
        reason, 
        from, 
        to, 
        after, 
        before, 
        if limit_number == 0 { None } else { Some(limit_number as i64) },
        Some(start as i64)
    ).await;
    
    (Status::Ok, (ContentType::JSON, serde_json::to_string(&transactions).unwrap()))
}

#[post("/transaction", data = "<body>")]
pub async fn create_transaction(key: Auth, db: &State<Arc<DBManager>>, body: Json<TransactionAPI>) -> StatusStringResponse {
    let db = db.client().await;

    let middleman_name = key.name.to_string();
    let middleman_id = hash_user_guild(&key.name, &key.name);

    let body = body.0;

    if body.amount > 0 {
        if let Some(from_id) = body.from.to_vp_id(&middleman_id) {
            if let Some(to_id) = body.to.to_vp_id(&middleman_id) {
                if (middleman_id == from_id) && db.get_profile_by_id(&from_id).await.is_none() {
                    log::info!("Creating a profile for '{}' because it didn't exist", middleman_name);
                    db.create_profile(&middleman_name, &middleman_name, VoiceProfileAPI::default()).await.ok();
                }

                if let Some(from) = db.get_profile_by_id(&from_id).await {
                    if let Some(to) = db.get_profile_by_id(&to_id).await {
                        if (from_id != middleman_id) && from.god.is_some() {
                            (Status::BadRequest, (ContentType::Plain, "You're trying to make transaction from other bot, please no".to_string()))
                        } else {
                            if from_id == to_id {
                                (Status::BadRequest, (ContentType::Plain, "Specify unique sender and receiver".to_string()))
                            } else {
                                let result = db.create_transaction(&from, &to, &middleman_name, body).await;

                                if let Ok(transaction) = result {
                                    (Status::Ok, (ContentType::JSON, serde_json::to_string(&transaction).unwrap()))
                                } else {
                                    (Status::BadRequest, (ContentType::Plain, "Request wasn't satisfying enough for database".to_string()))
                                }
                            }
                        }
                    } else {
                        (Status::BadRequest, (ContentType::Plain, "'to' profile wasn't found".to_string()))
                    }
                } else {
                    (Status::BadRequest, (ContentType::Plain, "'from' profile wasn't found".to_string()))
                }
            } else {
                (Status::BadRequest, (ContentType::Plain, "Specify 'to' profile correctly".to_string()))
            }
        } else {
            (Status::BadRequest, (ContentType::Plain, "Specify 'from' profile correctly".to_string()))
        }
    } else {
        (Status::BadRequest, (ContentType::Plain, "Amount shouldn't be less than 1".to_string()))
    }
}