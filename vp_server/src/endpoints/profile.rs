use std::sync::Arc;
use vp_core::model::profile::{VoiceProfile, VoiceProfileAPI, VoiceProfileAPIBulk};
use std::cmp::Ordering;
use std::time::{SystemTime, UNIX_EPOCH};
use crate::endpoints::{StatusStringResponse, Auth};
use rocket::State;
use rocket::http::{Status, ContentType};
use rocket::serde::json::Json;
use vp_core::db::manager::DBManager;
use vp_core::util::{list_to_tree_by_groups, list_to_tree_by_users};

#[get("/profile?<limit>&<page>&<view>")]
pub async fn get_profiles(db: &State<Arc<DBManager>>, limit: Option<i32>, page: Option<i32>, view: Option<String>) -> StatusStringResponse {
    let db = db.client().await;
    let profiles = db.get_profiles().await;

    let limit = limit.unwrap_or(50);
    let page = page.unwrap_or(0);
    let view = view.unwrap_or("list".to_string());

    let start = limit * page;
    let end = limit * (page + 1);

    if view == "tree" {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&list_to_tree_by_groups(&profiles)).unwrap()))
    } else {
        if limit <= 0 {
            (Status::Ok, (ContentType::JSON, serde_json::to_string(&profiles).unwrap()))
        } else {
            if profiles.len() > start as usize {
                let slice = &profiles[(start as usize)..(usize::min(end as usize, profiles.len()))];

                (Status::Ok, (ContentType::JSON, serde_json::to_string(&slice).unwrap()))
            } else {
                (Status::Ok, (ContentType::JSON, "[]".to_string()))
            }
        }
    }
}

#[get("/profile/guilds")]
pub async fn get_guilds(db: &State<Arc<DBManager>>) -> StatusStringResponse {
    let db = db.client().await;
    let profiles = db.get_guilds().await;

    (Status::Ok, (ContentType::JSON, serde_json::to_string(&profiles).unwrap()))
}

#[get("/profile/<guild_id>?<limit>&<page>&<view>", rank = 10)]
pub async fn get_guild_profiles(db: &State<Arc<DBManager>>, guild_id: &str, limit: Option<i32>, page: Option<i32>, view: Option<String>) -> StatusStringResponse {
    let db = db.client().await;
    let profiles = db.get_group_profiles(guild_id).await;

    let limit = limit.unwrap_or(50);
    let page = page.unwrap_or(0);
    let view = view.unwrap_or("list".to_string());

    let start = limit * page;
    let end = limit * (page + 1);

    if view == "tree" {
        if let Some(profiles) = profiles {
            (Status::Ok, (ContentType::JSON, serde_json::to_string(&list_to_tree_by_users(&profiles)).unwrap()))
        } else {
            (Status::NotFound, (ContentType::Plain, "Specified guild_id wasn't found".to_string()))
        }
    } else {
        if limit <= 0 {
            if let Some(profiles) = profiles {
                (Status::Ok, (ContentType::JSON, serde_json::to_string(&profiles).unwrap()))
            } else {
                (Status::NotFound, (ContentType::Plain, "Specified guild_id wasn't found".to_string()))
            }
        } else {
            if let Some(profiles) = profiles {
                if profiles.len() > start as usize {
                    let slice = &profiles[(start as usize)..(usize::min(end as usize, profiles.len()))];

                    (Status::Ok, (ContentType::JSON, serde_json::to_string(&slice).unwrap()))
                } else {
                    (Status::Ok, (ContentType::JSON, "[]".to_string()))
                }
            } else {
                (Status::NotFound, (ContentType::Plain, "Specified guild_id wasn't found".to_string()))
            }
        }
    }
}

#[get("/profile/guilds/<user_id>", rank = 1)]
pub async fn get_user_guilds(db: &State<Arc<DBManager>>, user_id: &str) -> StatusStringResponse {
    let db = db.client().await;
    let profiles = db.get_user_guilds(user_id).await;

    if let Some(profiles) = profiles {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profiles).unwrap()))
    } else {
        (Status::NotFound, (ContentType::Plain, "Specified user_id wasn't found".to_string()))
    }
}

#[get("/profile/<guild_id>/<user_id>", rank = 4)]
pub async fn get_profile(db: &State<Arc<DBManager>>, guild_id: &str, user_id: &str) -> StatusStringResponse {
    let db = db.client().await;
    let profile = db.get_profile(guild_id, user_id).await;

    if let Some(profile) = profile {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profile).unwrap()))
    } else {
        (Status::NotFound, (ContentType::Plain, "Specified profile wasn't found".to_string()))
    }
}

#[post("/profile/<guild_id>/<user_id>", data = "<body>", rank = 2)]
pub async fn create_profile(_key: Auth, db: &State<Arc<DBManager>>, guild_id: &str, user_id: &str, body: Option<Json<VoiceProfileAPI>>) -> StatusStringResponse {
    let db = db.client().await;

    let profile = db.create_profile(
        guild_id,
        user_id,
        {
            if let Some(json) = body {
                json.0.restify()
            } else {
                VoiceProfileAPI::default()
            }
        }
    ).await;

    if let Ok(profile) = profile {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profile).unwrap()))
    } else {
        (Status::InternalServerError, (ContentType::Plain, "Something went horribly wrong".to_string()))
    }
}

#[patch("/profile/<guild_id>/<user_id>", data = "<body>", rank = 2)]
pub async fn update_profile(_key: Auth, db: &State<Arc<DBManager>>, guild_id: &str, user_id: &str, body: Json<VoiceProfileAPI>) -> StatusStringResponse {
    let db = db.client().await;

    let profile = db.update_profile(
        guild_id,
        user_id,
        false,
        body.0.restify()
    ).await;

    if let Ok(profile) = profile {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profile).unwrap()))
    } else {
        (Status::NotFound, (ContentType::Plain, "Specified profile wasn't found".to_string()))
    }
}

#[patch("/profile/<guild_id>/<user_id>/add", data = "<body>")]
pub async fn update_profile_add(_key: Auth, db: &State<Arc<DBManager>>, guild_id: &str, user_id: &str, body: Json<VoiceProfileAPI>) -> StatusStringResponse {
    let db = db.client().await;

    let profile = db.update_profile(
        guild_id,
        user_id,
        true,
        body.0.restify()
    ).await;

    if let Ok(profile) = profile {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profile).unwrap()))
    } else {
        (Status::NotFound, (ContentType::Plain, "Specified profile wasn't found".to_string()))
    }
}

#[patch("/profile", data = "<body>")]
pub async fn update_profiles(_key: Auth, db: &State<Arc<DBManager>>, body: Json<Vec<VoiceProfileAPIBulk>>) -> StatusStringResponse {
    let db = db.client().await;

    let profile = db.update_profiles(
        false,
        body.0
    ).await;

    if let Ok(profile) = profile {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profile).unwrap()))
    } else {
        (Status::NotFound, (ContentType::Plain, "Some profile from specified wasn't found".to_string()))
    }
}

#[patch("/profile/add", data = "<body>")]
pub async fn update_profiles_add(_key: Auth, db: &State<Arc<DBManager>>, body: Json<Vec<VoiceProfileAPIBulk>>) -> StatusStringResponse {
    let db = db.client().await;

    let profile = db.update_profiles(
        true,
        body.0
    ).await;

    if let Ok(profile) = profile {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profile).unwrap()))
    } else {
        (Status::NotFound, (ContentType::Plain, "Some profile from specified wasn't found".to_string()))
    }
}

pub fn sort_profiles(profiles: &mut Vec<VoiceProfile>, by: &str) -> bool {
    let relevant_time_seconds = (SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs() - 129600) as u64;
    let relevant_time_millis = (SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis() - 129600000) as u64;

    let seconds_digits = relevant_time_seconds.to_string().len();

    match by {
        "points" => {
            profiles.sort_by(|a, b| {
                b.voicepoints.partial_cmp(&a.voicepoints).unwrap()
            });

            true
        }

        "levels" => {
            profiles.sort_by(|a, b| {
                let lvl = b.level.partial_cmp(&a.level).unwrap();

                if let Ordering::Equal = lvl {
                    b.experience.partial_cmp(&a.experience).unwrap()
                } else {
                    lvl
                }
            });

            true
        }

        "time" => {
            profiles.sort_by(|a, b| {
                if let Some(lhs) = b.timespent.get("global") {
                    if let Some(rhs) = a.timespent.get("global") {
                        lhs.0.partial_cmp(&rhs.0).unwrap()
                    } else {
                        Ordering::Greater
                    }
                } else {
                    Ordering::Less
                }
            });

            true
        }

        "text-level" => {
            profiles.sort_by(|a, b| {
                if let Some(a) = &a.text {
                    if let Some(b) = &b.text {
                        let lvl = b.level.partial_cmp(&a.level).unwrap();

                        if let Ordering::Equal = lvl {
                            b.experience.partial_cmp(&a.experience).unwrap()
                        } else {
                            lvl
                        }
                    } else {
                        Ordering::Less
                    }
                } else {
                    Ordering::Greater
                }
            });

            true
        }

        "text-message" => {
            profiles.sort_by(|a, b| {
                if let Some(a) = &a.text {
                    if let Some(b) = &b.text {
                        b.message_count.partial_cmp(&a.message_count).unwrap()
                    } else {
                        Ordering::Less
                    }
                } else {
                    Ordering::Greater
                }
            });

            true
        }

        "pray-streak" => {
            profiles.sort_by(|a, b| {
                if let Some(a) = &a.pray {
                    if let Some(b) = &b.pray {
                        // Checking if expired
                        if a.date < if a.date.to_string().len() > seconds_digits { relevant_time_millis } else { relevant_time_seconds } {
                            if b.date < if b.date.to_string().len() > seconds_digits { relevant_time_millis } else { relevant_time_seconds } {
                                Ordering::Equal
                            } else {
                                Ordering::Greater
                            }
                        } else {
                            if b.date < if b.date.to_string().len() > seconds_digits { relevant_time_millis } else { relevant_time_seconds } {
                                Ordering::Less
                            } else {
                                b.streak.partial_cmp(&a.streak).unwrap_or(Ordering::Equal)
                            }
                        }
                    } else {
                        Ordering::Less
                    }
                } else {
                    if b.pray.is_none() {
                        Ordering::Less
                    } else {
                        Ordering::Equal
                    }
                }
            });

            true
        }

        _ => false,
    }
}

#[get("/profile/top?<by>&<amount>&<offset>", rank = 3)]
pub async fn get_top(db: &State<Arc<DBManager>>, by: String, amount: Option<i32>, offset: Option<i32>) -> StatusStringResponse {
    let db = db.client().await;

    let mut profiles = db.get_profiles().await;

    if !sort_profiles(&mut profiles, &by) {
        return (Status::BadRequest, (ContentType::Plain, "Invalid by query parameter".to_string()))
    }

    let amount = amount.unwrap_or(5);
    let offset = offset.unwrap_or(0);

    let start = offset;
    let end = amount + offset;

    if amount <= 0 {
        (Status::Ok, (ContentType::JSON, serde_json::to_string(&profiles).unwrap()))
    } else {
        if profiles.len() > start as usize {
            let slice = &profiles[(start as usize)..(usize::min(end as usize, profiles.len()))];

            (Status::Ok, (ContentType::JSON, serde_json::to_string(&slice).unwrap()))
        } else {
            (Status::Ok, (ContentType::JSON, "[]".to_string()))
        }
    }
}

#[get("/profile/<guild_id>/top?<by>&<amount>&<offset>", rank = 3)]
pub async fn get_guild_top(db: &State<Arc<DBManager>>, guild_id: &str, by: String, amount: Option<i32>, offset: Option<i32>) -> StatusStringResponse {
    let db = db.client().await;

    let profiles = db.get_group_profiles(guild_id).await;

    let amount = amount.unwrap_or(5);
    let offset = offset.unwrap_or(0);

    let start = offset;
    let end = amount + offset;


    if let Some(mut profiles) = profiles {
        // Sorting by voicepoints descenting
        if !sort_profiles(&mut profiles, &by) {
            return (Status::BadRequest, (ContentType::Plain, "Invalid by query parameter".to_string()))
        }

        if amount <= 0 {
            (Status::Ok, (ContentType::JSON, serde_json::to_string(&profiles).unwrap()))
        } else {
            if profiles.len() > start as usize {
                let slice = &profiles[(start as usize)..(usize::min(end as usize, profiles.len()))];

                (Status::Ok, (ContentType::JSON, serde_json::to_string(&slice).unwrap()))
            } else {
                (Status::Ok, (ContentType::JSON, "[]".to_string()))
            }
        }
    } else {
        (Status::NotFound, (ContentType::Plain, "Specified guild_id wasn't found".to_string()))
    }
}