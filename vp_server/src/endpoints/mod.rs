pub mod profile;
pub mod transaction;
pub mod statistics;

use comrak::ComrakOptions;
use std::sync::Arc;
use rocket::http::{Status, ContentType};
use rocket::{Request, State};
use rocket::request::{FromRequest, self};
use rocket::outcome::Outcome;
use vp_core::db::manager::DBManager;

pub type StatusStringResponse = (Status, (ContentType, String));

#[get("/")]
pub async fn root() -> StatusStringResponse {
    let mut options = ComrakOptions::default();
    options.render.unsafe_ = true;

    let markdown = comrak::markdown_to_html(
        include_str!("documentation/doc.md"),
        &options
    );

    let html = format!(r#"<head>{}<title>{}</title><style>{}</style></head><div class="markdown-body">{}</div>"#,
        r#"<meta name="viewport" content="width=device-width, initial-scale=1.0">"#,
        "Voice Profile Server",
        include_str!("documentation/doc.css"),
        markdown);

    (Status::Ok, (ContentType::HTML, html))
}

#[derive(Clone, Debug)]
pub struct Auth {
    pub key: String,
    pub name: String,
}

#[derive(Debug)]
pub enum AuthError {
    Missing,
    Invalid
}

#[rocket::async_trait]
impl<'a> FromRequest<'a> for Auth {
    type Error = AuthError;

    async fn from_request(request: &'a Request<'_>) -> request::Outcome<Self, Self::Error> {
        let db = request.guard::<&State<Arc<DBManager>>>().await.unwrap().inner();
        let key = request.headers().get_one("Authorization");

        let db = db.client().await;

        if let Some(key) = key {
            let name = db.check_key_directly(key).await;

            if let Some(name) = name {
                Outcome::Success(Auth {
                    key: key.to_string(),
                    name
                })
            } else {
                Outcome::Error((Status::Unauthorized, AuthError::Invalid))
            }
        } else {
            Outcome::Error((Status::Unauthorized, AuthError::Missing))
        }
    }
}