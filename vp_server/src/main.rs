#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_include_static_resources;

mod endpoints;
mod cors;

use std::sync::Arc;
use flexi_logger::{LogSpecification, Logger, FileSpec, Criterion, Age, Naming, Cleanup, WriteMode, Duplicate, colored_default_format};
use log::LevelFilter;
use rocket::{Build, Rocket};
use vp_core::db::manager::DBManager;
use crate::cors::{auto_gen_options, CORS};

static_response_handler! {
    "/favicon.ico" => favicon => "favicon",
}

#[launch]
async fn rocket() -> Rocket<Build> {
    // Logger
    let mut builder = LogSpecification::builder();
    builder
        .default(LevelFilter::Off)
        .module("tokio_postgres", LevelFilter::Debug)
        .module("rocket", LevelFilter::Info)
        .module("vp_core", LevelFilter::Trace)
        .module("vp_server", LevelFilter::Trace);

    Logger::with(builder.build())
        .log_to_file(
            FileSpec::default()
                .directory("log")
                .basename("vp_server_log")
                .suffix("log")
        )
        .rotate(
            Criterion::Age(Age::Day),
            Naming::Timestamps,
            Cleanup::KeepLogAndCompressedFiles(3, 90)
        )
        .write_mode(WriteMode::BufferAndFlush)
        .duplicate_to_stdout(Duplicate::All)
        .format(colored_default_format)
        .start().unwrap();

    // Database
    let db = DBManager::new().await;

    // Web Server
    let rocket = rocket::build()
        .attach(
            CORS
        )
        .manage(Arc::new(db))

        .mount("/", routes![ // Transactions
            endpoints::transaction::get_transactions,
            endpoints::transaction::create_transaction
        ])

        .mount("/", routes![ // Profiles
            endpoints::profile::get_profiles,
            endpoints::profile::get_guild_profiles,
            endpoints::profile::get_guilds,
            endpoints::profile::get_user_guilds,
            endpoints::profile::get_profile,

            endpoints::profile::create_profile,

            endpoints::profile::update_profile,
            endpoints::profile::update_profile_add,
            endpoints::profile::update_profiles,
            endpoints::profile::update_profiles_add,

            endpoints::profile::get_top,
            endpoints::profile::get_guild_top,

            endpoints::statistics::get_statistics,
            endpoints::statistics::get_balance_graph
        ]);

    auto_gen_options(rocket)
        .attach(static_resources_initializer!(
            "favicon" => "src/endpoints/documentation/favicon.ico",
        ))
        .mount("/", routes![endpoints::root])
        .mount("/", routes![favicon])
}
